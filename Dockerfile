FROM php:7.2-fpm-alpine
EXPOSE "80"
RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"
RUN apk update && apk upgrade && apk add --no-cache $PHPIZE_DEPS && pecl install xdebug && docker-php-ext-enable xdebug &&\
 apk add --no-cache jpeg-dev freetype-dev libpng-dev libxslt-dev openjpeg-dev libxml2-dev curl-dev libcurl libzip-dev &&\
  docker-php-ext-configure zip --with-libzip && docker-php-ext-configure gd --with-jpeg-dir=/usr/include &&\
   docker-php-ext-install -j5 gd mbstring phar xml xsl opcache iconv json zip curl pdo pdo_mysql mysqli &&\
    docker-php-ext-enable mysqli &&\
    echo 'xdebug.remote_connect_back=1' >> $PHP_INI_DIR/conf.d/docker-php-ext-xdebug.ini &&\
    echo 'xdebug.remote_enable=1' >> $PHP_INI_DIR/conf.d/docker-php-ext-xdebug.ini
CMD [ "php", "-S", "0.0.0.0:80" ]
