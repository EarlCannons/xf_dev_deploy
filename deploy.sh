#!/usr/bin/env bash

read -p "Email: " email
read -p "User: " user
read -p "Password: " pass

webroot=./code
xfmysqlusr=xf_usr
xfdbname=xf_dev
mysqlupw=xf_pw
mysql_host=xf2-dev_xf2-db_1


for filename in official_zip_files/*.zip; do
 pushd official_zip_files
 mkdir extract
 pushd extract
 unzip ../../$filename upload/*
 sleep 1
 rsync -av upload/ ../../code/
 popd
 rm -rf extract
 popd
done

cp $webroot/src/config.php.default $webroot/src/config.php

sed -i "s/\$config\['db'\]\['username'\] = '';/\$config\['db'\]\['username'\] = '$xfmysqlusr';/" $webroot/src/config.php
sed -i "s/\$config\['db'\]\['dbname'\] = '';/\$config\['db'\]\['dbname'\] = '$xfdbname';/" $webroot/src/config.php
sed -i "s/\$config\['db'\]\['password'\] = '';/\$config\['db'\]\['password'\] = '$mysqlupw';/" $webroot/src/config.php
sed -i "s/\$config\['db'\]\['host'\] = 'localhost';/\$config\['db'\]\['host'\] = '$mysql_host';/" $webroot/src/config.php


docker-compose up -d
read -t 30 -p $'Press [Enter] key to install xenforo from CLI \n or press CTRL-C to abort. \nThen you can visit http://localhost and start installing process on web browser.\n'
if [ $? -eq 0 ]
then
  echo "Waiting for a moment.."
  sleep 30
fi
/usr/bin/docker exec -it xf2-dev_xf2_1 php cmd.php xf:install --user $user --email=$email --password=$pass --url=http://localhost --skip-statistics -n